---
Title: Areas of Focus
---
# Areas of Focus

## life purpose

| Definition | Focus |
| ---        | ---   |
|            |       |

## family and relationships

| Definition | Focus |
| ---        | ---   |
|            |       |

## health

| Definition | Focus |
| ---        | ---   |
|            |       |

## self development

| Definition | Focus |
| ---        | ---   |
|            |       |

## finances

| Definition | Focus |
| ---        | ---   |
|            |       |

## career and business

| Definition | Focus |
| ---        | ---   |
|            |       |

## lifestyle and life experiences

| Definition | Focus |
| ---        | ---   |
|            |       |

## spirituality

| Definition | Focus |
| ---        | ---   |
|            |       |

