#!/usr/bin/bash
# NOTE: chatgpt-cli is installed from source.
# Maybe create a pkgbuild for it
#
# Config file location ~/.config/bash/configurations/chatgpt-cli.bash
printf "%s\n" "Installing chatgpt-cli"
curl -L -o chatgpt https://github.com/kardolus/chatgpt-cli/releases/latest/download/chatgpt-linux-386 && chmod +x chatgpt && sudo mv chatgpt /usr/local/bin/

[[ -f "$HOME"/.config/bash/configurations/chatgpt-cli.bash.local ]] || cp "$HOME"/.config/user-data/packages/configurations/chatgpt/chatgpt-cli.bash.local "$HOME"/.config/bash/configurations
[[ -f "$HOME"/.config/bash/configurations/chatgpt-cli.bash ]] || cp "$HOME"/.config/user-data/packages/configurations/chatgpt/chatgpt-cli.bash "$HOME"/.config/bash/configurations

cat "$HOME"/.config/bash/configurations/chatgpt-cli.bash.local
nvim "$HOME"/.config/bash/configurations/chatgpt-cli.bash.local

# chatgpt expects it's configuration in home.
[[ -d "$HOME"/.local/share/do-user/chatgpt-cli/ ]] || mkdir -p "$HOME"/.local/share/do-user/chatgpt-cli/
[[ -f "$HOME"/.local/share/do-user/chatgpt-cli/config.yaml ]] || cp "$HOME"/.config/user-data/packages/configurations/chatgpt/config.yaml.gpt-4o "$HOME"/.local/share/do-user/chatgpt-cli/config.yaml
[[ -d "$HOME"/.cache/chatgpt-cli/history ]] || mkdir -p "$HOME"/.cache/chatgpt-cli/history
[[ -L "$HOME"/.local/share/do-user/chatgpt-cli/history ]] || ln -s "$HOME"/.cache/chatgpt-cli/history/ "$HOME"/.local/share/do-user/chatgpt-cli/history
[[ -L "$HOME"/.chatgpt-cli ]] || ln -s "$HOME"/.local/share/do-user/chatgpt-cli/ "$HOME"/.chatgpt-cli

printf "%s\n" "Chatgpt-cli is setup"
