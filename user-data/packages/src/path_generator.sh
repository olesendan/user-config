#!/usr/bin/bash
# configurations for path_generator.
# NOTE: path_generator is installed from source.
# Maybe create a pkgbuild for it
#
pack_start_dir="$PWD"
printf "%s\n" "Installing path_generator"
if [[ -d ./path_generator ]]; then
	printf "%s\n" "session exists"
	cd ./path_generator || return
	git pull
else
	printf "%s\n" "session do not exists"
	git clone https://gitlab.com/olesendan/path_generator.git
	cd ./path_generator || return
fi
chmod +x install.sh
./install.sh
cd "$pack_start_dir" || return
echo "$PWD"

printf "%s\n" "path_generator is setup"
