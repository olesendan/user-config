#!/usr/bin/bash
# configurations for tmux_session_maker
# NOTE: tmux_session_maker is installed from source.
# Maybe create a pkgbuild for it
#
# Config file location ~/.config/bash/configurations/tmux_session_maker.bash
pack_start_dir="$PWD"
printf "%s\n" "Installing tmux_session_maker"
# echo "$PWD"
# ls ./
if [[ -d ./tmux_session_maker ]]; then
	printf "%s\n" "session exists"
	cd ./tmux_session_maker || return
	# echo "$PWD"
	git pull
else
	printf "%s\n" "session do not exists"
	git clone https://gitlab.com/olesendan/tmux_session_maker.git
	cd ./tmux_session_maker || return
fi
# echo "$PWD"
# ls ./
chmod +x install.sh
# cat ./install.sh
./install.sh
cd "$pack_start_dir" || return
echo "$PWD"

printf "%s\n" "tmux_session_maker is setup"
