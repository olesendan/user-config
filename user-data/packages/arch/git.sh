# configuration file for git.
# generic config file location is ~/.config/git/config

CONFIG_FILE="$HOME/.config/git/config.local"
if [[ ! -f $CONFIG_FILE ]]; then
	mkdir -p "$(dirname "$CONFIG_FILE")"
	cp "$HOME"/.config/user-data/packages/configurations/git/config "$CONFIG_FILE"
fi
cat "$CONFIG_FILE"
read -r -p 'Do you want to edit information (yes/no)? ' answer
[[ $answer == [Yy]* ]] && nvim "$CONFIG_FILE"
