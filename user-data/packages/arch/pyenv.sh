# configuration file for pyenv.
# configurations for bash is located at ~/.config/bash/configurations/pyenv.bash

# move actual data out of root
mkdir -pv "$HOME"/.local/share/python/ipython
mkdir -pv "$HOME"/.cache/python
if [[ ! -L "$HOME"/.python_history ]]; then
	mv "$HOME"/.python_history "$HOME"/.cache/python/history
	ln -s "$HOME"/.cache/python/history "$HOME"/.python_history
fi
if [[ ! -L "$HOME"/.ipython ]]; then
	mv "$HOME"/.ipython/* "$HOME"/.local/share/python/ipython/
	rm -r --force "$HOME"/.ipython
	ln -s "$HOME"/.local/share/python/ipython/ "$HOME"/.ipython
fi

if [[ ! -f "$HOME"/.config/bash/configurations/pyenv.bash.local ]]; then
	touch "$HOME"/.config/bash/configurations/pyenv.bash.local
	{
		echo "# Machine specific pyenv configurations"
		echo "# See ~/.config/bash/configurations/pyenv.bash for syntax"
	} >"$HOME"/.config/bash/configurations/pyenv.bash.local
fi
cat "$HOME"/.config/bash/configurations/pyenv.bash.local
read -r -p 'Do you want to edit information (yes/no)? ' answer
[[ $answer == [Yy]* ]] && nvim "$HOME"/.config/bash/pyenv.bash.local

if [[ ! -f "$HOME"/.config/python/requirements.in.local ]]; then
	{
		echo "# machine specific libraries for python env"
		echo "# do-user libraries set from ~/.config/python/requirements.in"
	} >"$HOME"/.config/python/requirements.in.local
fi
cat "$HOME"/.config/python/requirements.in.local
read -r -p 'Do you want to edit information (yes/no)? ' answer
[[ $answer == [Yy]* ]] && nvim "$HOME"/.config/python/requirements.in.local

cd "$HOME" || return 1
selected=$(pyenv install --list | fzf)
selected=${selected//" "/""}
pyenv install "$selected"
pyenv global "$selected"
pip install --upgrade pip
pip install pip-tools

pip-compile -o "$HOME"/.local/share/python/requirements.txt "$HOME"/.config/python/requirements.in "$HOME"/.config/python/requirements.in.local
pip-sync "$HOME"/.local/share/python/requirements.txt
cd "$HOME"/.config || return 1
