#!/usr/bin/bash
# configuration file for nvim
# bash configurations located at ~/.config/bash/configurations/neovim.bash

if [[ ! -f "$HOME"/.config/nvim/lua/user/plugins/local/init.lua ]]; then
    mkdir -p "$HOME"/.config/nvim/lua/user/plugins/local
    cp "$HOME"/.config/user-data/packages/configurations/nvim/plugins.lua.local "$HOME"/.config/nvim/lua/user/plugins/local/init.lua
fi

cat "$HOME"/.config/nvim/lua/user/plugins/local/init.lua
read -r -p 'Do you want to add plugins now (yes/no)?: ' answer
[[ $answer == [Yy]* ]] && nvim "$HOME"/.config/nvim/lua/user/plugins/local/init.lua
printf "%s\n" "Sourced neovim.sh"
