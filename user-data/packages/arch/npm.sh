# configuration of npm
if [[ ! -L "$HOME"/.npm ]]; then
	mkdir "$HOME"/.local/share/npm
	mv "$HOME"/.npm/* "$HOME"/.local/share/npm
	rm -r --force "$HOME"/.npm
	ln -s "$HOME"/.local/share/npm/ "$HOME"/.npm
fi

# requirement from neovim
sudo npm install -g neovim
