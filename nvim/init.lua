require("user.options")
require("user.keymaps")
require("user.autocommands")
require("user.lazy")
vim.g.netrw_liststyle = 3
