vim.notify('Loading help ftplugin settings', vim.log.levels['INFO'])
local opt = vim.opt_local
opt.expandtab = true
opt.tabstop = 2
opt.shiftwidth = 4
opt.softtabstop = 4
opt.colorcolumn = '79'
