local opt = vim.opt
opt.conceallevel = 2

vim.api.nvim_set_keymap("n", "<leader>bt", "vip :!tr -s ' ' | column -t -s '|' -o '|'<CR>", { desc = "Aligning table" })
