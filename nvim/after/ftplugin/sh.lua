vim.notify("Loading sh ftplugin", vim.log.levels["INFO"])
local opt = vim.opt
opt.expandtab = true
opt.shiftwidth = 4
opt.tabstop = 4
opt.shiftwidth = 4
opt.softtabstop = 4
opt.colorcolumn = "79"

opt.foldmethod = "indent"

-- snippets
local ls = require("luasnip")

local snippet = ls.snippet -- s
local snippet_node = ls.snippet_node -- sn
local text_node = ls.text_node -- t
local insert_node = ls.insert_node -- i
local ft = "sh"

ls.add_snippets(ft, {
	snippet("printf_s", {
		text_node('printf "%s\\n" "'),
		insert_node(1),
		text_node('"'),
	}),
	snippet("funct", {
		text_node("function "),
		insert_node(1),
		text_node({ "() {", "\t" }),
		insert_node(2),
		text_node({ "", "}", "", "" }),
	}),
})
