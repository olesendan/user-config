vim.notify('Loading sql ftplugin', vim.log.levels['INFO'])
local opt = vim.opt_local
opt.expandtab = true
opt.shiftwidth = 4
opt.tabstop = 4
opt.shiftwidth = 4
opt.softtabstop = 4
opt.colorcolumn = '79'

opt.foldmethod = 'indent'
