vim.notify('Loading which-key config', vim.log.levels['INFO'])
vim.o.timeout = true
vim.o.timeoutlen = 500
local wk = require 'which-key'
wk.setup {
  window = {
    border = 'single',
    position = 'bottom',
    winblend = 40,
  },
}

wk.register {
  ['<leader>t'] = { name = '[T]est', _ = 'which_key_ignore' },
  ['<leader>h'] = { name = '[H]elp', _ = 'which_key_ignore' },
  ['<leader>hk'] = { name = '[K]eymaps', _ = 'which_key_ignore' },
}
-- vim.keymap.set(
--   'n',
--   '<leader>hkw',
--   ':WhichKey<CR>',
--   { desc = 'Open which key TUI' }
-- )
