vim.notify('Load git config', vim.log.levels['INFO'])

local key = function(mode, lhs, rhs, opts)
  opts.desc = 'Git: ' .. opts.desc
  local lhs_g = '<leader>g' .. lhs
  vim.keymap.set(mode, lhs_g, rhs, opts)
end
local nkey = function(lhs, rhs, opts) key('n', lhs, rhs, opts) end
local diffview = require 'diffview'
diffview.setup {}

local git = require 'gitsigns'
git.setup {
  on_attach = function(bufnr)
    nkey('p', git.prev_hunk, { desc = 'Go to prev hunk' })
    nkey('n', git.next_hunk, { desc = 'Go to next hunk' })
    nkey('h', git.preview_hunk, { desc = 'preview hunk' })
  end,
}

local fzf = require 'fzf-lua'
local fterm = require 'FTerm'
local wk = require 'which-key'
wk.register { ['<leader>g'] = { name = 'Version control' } }
nkey('c', fterm.toggle, { desc = 'Commit in Cus term' })
wk.register { ['<leader>gd'] = { name = 'DiffView' } }
nkey('do', '<CMD>DiffviewOpen<CR>', { desc = 'Open DiffView current buffer' })
nkey('dc', '<CMD>DiffviewClose<CR>', { desc = 'Close DiffView current buffer' })
wk.register { ['<leader>gs'] = { name = 'fzf search' } }
nkey('sb', fzf.git_branches, { desc = 'git branches' })
nkey('scb', fzf.git_bcommits, { desc = 'git commits buffer' })
nkey('scp', fzf.git_commits, { desc = 'git commits' })
nkey('<leader>sf', fzf.git_files, { desc = 'git files' })
nkey('<leader>sss', fzf.git_status, { desc = 'git status' })
nkey('<leader>sst', fzf.git_stash, { desc = 'git stash' })
nkey('<leader>st', fzf.git_tags, { desc = 'git tags' })
