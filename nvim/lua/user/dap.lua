vim.notify('Load dap config', vim.log.levels['INFO'])

-- NOTE: python debugpy are managed with pyenv for the user
-- 'debugpy',
local adapters_to_install = {}
require('mason').setup {}
local fzf = require 'fzf-lua'
local registry = require 'mason-registry'
registry.refresh(function()
  for _, name in pairs(adapters_to_install) do
    vim.print('Checking dap ' .. name)
    local package = registry.get_package(name)
    if not registry.is_installed(name) then
      vim.print('Installing dap ' .. name)
      package:install()
    end
  end
end)
local wk = require 'which-key'
-- require('nvim-dap-virtual-text').setup()
require('nvim-dap-repl-highlights').setup()
local python_path = function()
  local py_vers = vim.fs.normalize '~/.local/share/pyenv/versions' .. '/'
  local py_ver = vim.fn.system { 'pyenv', 'global' }
  local py_abs = py_vers .. py_ver:gsub('\n', '') .. '/bin/python'
  return py_abs
end
local dap = require 'dap'
local widgets = require 'dap.ui.widgets'

local dap_py = require 'dap-python'
dap_py.setup(python_path())

local key = function(mode, lhs, rhs, opts)
  opts.desc = 'DAP: ' .. opts.desc
  vim.keymap.set(mode, lhs, rhs, opts)
end
local nkey = function(lhs, rhs, opts) key('n', lhs, rhs, opts) end
local nvkey = function(lhs, rhs, opts) key({ 'n', 'v' }, lhs, rhs, opts) end
wk.register { ['<leader>t'] = { name = '[T]est/debug' } }
wk.register { ['<leader>ts'] = { name = 'fzf search' } }
nkey('<leader>tb', dap.toggle_breakpoint, { desc = 'Toggle breakpoint' })
nkey(
  '<leader>tB',
  function() dap.set_breakpoint(vim.fn.input 'Condition for breakpoint: ') end,
  { desc = 'set conditional breakpoint' }
)
wk.register { ['<leader>tc'] = { name = 'step/continue run' } }
nkey('<leader>tcc', dap.continue, { desc = 'continue test' })
nkey('<leader>tci', dap.step_into, { desc = 'step into' })
nkey('<leader>tcI', dap.step_out, { desc = 'step out' })
nkey('<leader>tco', dap.step_over, { desc = 'step over' })
nkey('<leader>td', dap_py.test_method, { desc = 'test closest method' })
nkey(
  '<leader>tD',
  function() dap_py.test_method { config = { justMyCode = false } } end,
  { desc = 'test closest method "justMyCode"' }
)
nvkey('<leader>tp', function() widgets.preview() end, { desc = 'preview' })
local toggle_dap_repl = function()
  dap.repl.toggle()
  vim.cmd 'wincmd p'
  local filetype = vim.api.nvim_get_option_value('filetype', {})
  if filetype == 'dap-repl' then vim.cmd 'startinsert' end
end
nkey('<leader>tr', function() toggle_dap_repl() end, { desc = 'Toggle repl' })
nkey('<leader>tsb', fzf.dap_breakpoints, { desc = 'list breakpoints' })
nkey('<leader>tsc', fzf.dap_commands, { desc = 'list nvim-dap' })
nkey('<leader>tsf', fzf.dap_frames, { desc = 'jump to frame' })
nkey('<leader>tss', fzf.dap_configurations, { desc = 'list debug settings' })
nkey('<leader>tsv', fzf.dap_variables, { desc = 'active session vars.' })
wk.register { ['<leader>tw'] = { name = 'widgets' } }
local scopes_float = nil
nkey('<leader>tws', function()
  if scopes_float then
    scopes_float.toggle()
  else
    scopes_float = widgets.centered_float(widgets.scopes)
  end
end, { desc = 'widget scopes' })
local threads_float = nil
nkey('<leader>twt', function()
  if threads_float then
    threads_float.toggle()
  else
    threads_float = widgets.centered_float(widgets.threads)
  end
end, { desc = 'widget threads' })
nkey('<leader>tx', function()
  dap.terminate({}, {}, function()
    local win_info = vim.fn.getwininfo()
    local win_term = vim.tbl_filter(function(value)
      if value.terminal == 1 then
        return value
      else
        return false
      end
    end, vim.tbl_values(win_info))
    if win_term[1] then
      vim.api.nvim_buf_delete(win_term[1].bufnr, { force = true })
    else
      return nil
    end
  end)
end, { desc = 'Terminate session' })
dap.defaults.fallback.terminal_win_cmd = '50vsplit new'
