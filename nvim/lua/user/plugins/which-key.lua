vim.notify('Loading which-key modules', vim.log.levels[INFO])
return {
  {
    'folke/which-key.nvim',
    event = 'VimEnter',
    opts = {
      window = {
        border = 'single',
      },
    },
    config = function()
      local wk = require 'which-key'
      wk.register {
        ['g'] = { name = '+goto' },
        ['gz'] = { name = '+surround' },
        [']'] = { name = '+next' },
        ['['] = { name = '+prev' },
      }
    end,
  },
}
