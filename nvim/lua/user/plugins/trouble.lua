vim.notify("Loading troble modules", vim.log.levels["INFO"])
return {
	{
		"folke/trouble.nvim",
		opts = {},
		config = function()
			require("user.diagnostic")
		end,
	},
}
