vim.notify("Load ui modules", vim.log.levels["INFO"])
return {
	{
		"lukas-reineke/indent-blankline.nvim",
		main = "ibl",
		opts = {},
		config = function()
			require("user.ui")
		end,
	},
}
