vim.notify("Loading fzf-lua modules", vim.log.levels[INFO])
return {
	{
		"ibhagwan/fzf-lua",
		config = function()
			require("user.fzf-lua")
		end,
	},
}
