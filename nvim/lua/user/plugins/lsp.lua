vim.notify("Loading lsp modules", vim.log.levels["INFO"])
return {
	{
		"neovim/nvim-lspconfig",
		dependencies = {
			{ "folke/lazydev.nvim", ft = "lua", opts = {} },
			"williamboman/mason.nvim",
			{ "j-hui/fidget.nvim", opts = {} },
			"b0o/SchemaStore.nvim",
		},
		config = function()
			require("user.lsp")
		end,
	},
}
