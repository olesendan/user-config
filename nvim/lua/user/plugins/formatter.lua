vim.notify("Load formatting modules", vim.log.levels["INFO"])
return {
	{
		"stevearc/conform.nvim",
		dependencies = {
			"williamboman/mason.nvim",
		},
		config = function()
			require("user.formatter")
		end,
	},
}
