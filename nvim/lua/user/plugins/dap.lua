vim.notify("Load dap modules", vim.log.levels["INFO"])
return {
	{
		"mfussenegger/nvim-dap",
		dependencies = {
			"mfussenegger/nvim-dap-python",
			-- 'theHamsta/nvim-dap-virtual-text',
			"LiadOz/nvim-dap-repl-highlights",
			"williamboman/mason.nvim",
		},
		config = function()
			require("user.dap")
		end,
	},
}
