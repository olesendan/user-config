return {
  {
    'numToStr/FTerm.nvim',
    opts = {},
    config = function(_, opts)
      local fterm = require 'FTerm'
      fterm.setup(opts)
      vim.keymap.set('n', '<leader>tt', fterm.toggle, { desc = 'Toggle Fterm' })
      vim.keymap.set(
        't',
        '<leader>tt',
        "<C-\\><C-n><CMD>lua require('FTerm').toggle()<CR>",
        { desc = 'Toggle Fterm' }
      )
    end,
  },
}
