local M = {
  {
    'folke/tokyonight.nvim',
    lazy = false,
    opts = {
      style = 'night',
      on_colors = function(colors) colors.border = '#565f89' end,
    },
    config = function(_, opts)
      require('tokyonight').setup(opts)
      vim.cmd.colorscheme { 'tokyonight' }
    end,
    priority = 1000,
  },
}
return M
