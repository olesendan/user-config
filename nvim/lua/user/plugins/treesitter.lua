vim.notify("Load treesitter modules", vim.log.levels["INFO"])

return {
	{
		"nvim-treesitter/nvim-treesitter",
		build = ":TSUpdate",
		config = function()
			require("user.treesitter")
		end,
	},
}
