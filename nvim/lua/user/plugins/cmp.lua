vim.notify("Loading nvim-cmp modules", vim.log.levels["INFO"])
return {
	{
		"hrsh7th/nvim-cmp",
		event = { "InsertEnter", "CmdlineEnter" },
		-- NOTE: cmp for vim-dadbod when implemented
		dependencies = {
			{ "folke/lazydev.nvim", ft = "lua", opts = {} },
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-cmdline",
			"hrsh7th/cmp-path",
			"hrsh7th/cmp-nvim-lsp",
			"hrsh7th/cmp-nvim-lsp-document-symbol",
			"hrsh7th/cmp-nvim-lsp-signature-help",
			{ "L3MON4D3/Luasnip", build = "make install_jsregexp" },
			"onsails/lspkind.nvim",
			"rcarriga/cmp-dap",
			"saadparwaiz1/cmp_luasnip",
		},
		config = function()
			require("user.cmp")
		end,
	},
}
