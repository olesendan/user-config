vim.notify("Loading comment modules", vim.log.levels["INFO"])
return {
	{
		"folke/todo-comments.nvim",
		event = "VimEnter",
		dependencies = { "nvim-lua/plenary.nvim" },
		config = function()
			require("user.comment")
		end,
	},
}
