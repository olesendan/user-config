vim.notify("Load git modules", vim.log.levels["INFO"])
local M = {
	{
		"lewis6991/gitsigns.nvim",
		dependencies = {
			"sindrets/diffview.nvim",
		},
		config = function()
			require("user.git")
		end,
	},
}
return M
