vim.notify("Load linter modules", vim.log.levels["INFO"])
return {
	{
		"mfussenegger/nvim-lint",
		dependencies = {
			"williamboman/mason.nvim",
		},
		config = function()
			require("user.linter")
		end,
	},
}
