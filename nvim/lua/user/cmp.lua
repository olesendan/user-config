vim.notify('Loading cmp config', vim.log.levels['INFO'])
-- vim.opt.completeopt = { 'menu,menuone,noinsert' }
vim.opt.completeopt = { 'menu,menuone,noselect' }
vim.opt.shortmess:append 'c'

local lspkind = require 'lspkind'
lspkind.init {}
local luasnip = require 'luasnip'
require('luasnip.loaders.from_vscode').lazy_load()
local cmp = require 'cmp'
cmp.setup {
  enabled = function()
    return vim.api.nvim_buf_get_option(0, 'buftype') ~= 'prompt'
      or require('cmp_dap').is_dap_buffer()
  end,
  formatting = {
    format = lspkind.cmp_format {
      mode = 'symbol_text',
      menu = {
        buffer = '[buf]',
        nvim_lsp = '[LSP]',
        luasnip = '[Lsnip]',
        nvim_lua = '[Lua]',
      },
    },
  },
  view = {
    docs = { auto_open = true },
    entries = { name = 'custom', selection_order = 'near_cursor' },
  },
  snippet = {
    expand = function(args) luasnip.lsp_expand(args.body) end,
  },
  window = {
    completion = cmp.config.window.bordered(),
    documentation = cmp.config.window.bordered(),
  },
  sources = cmp.config.sources {
    {
      name = 'nvim_lsp',
      priority = 1,
      -- group_index = 1,
    },
    { name = 'nvim_lsp_signature_help', priority = 2 },
    {
      name = 'luasnip',
      keyword_length = 2,
      -- group_index = 2,
    },
    {
      name = 'path',
      -- group_index = 3,
    },
    { name = 'buffer' },
    {
      name = 'treesitter',
      keyword_length = 3,
      max_item_count = 10,
      -- group_index = 1,
    },
    {
      name = 'nvim-cmp',
      priority = 1,
      -- group_index = 1,
    },
    {
      name = 'cmdline',
      -- group_index = 3,
    },
    {
      name = 'nvim_lua',
      -- group_index = 3,
    },
    { name = 'lazydev', group_index = 0 },
  },
  mapping = cmp.mapping.preset.insert {
    ['<C-n>'] = cmp.mapping.select_next_item {
      behavior = cmp.SelectBehavior.Insert,
    },
    ['<C-p>'] = cmp.mapping.select_prev_item {
      behavior = cmp.SelectBehavior.Insert,
    },
    ['<C-y>'] = cmp.mapping(
      cmp.mapping.confirm {
        -- behavior = cmp.ConfirmBehavior.Insert,
        select = true,
      },
      { 'i', 'c' }
    ),
    ['<C-e>'] = cmp.mapping.abort(),
    ['<C-b>'] = cmp.mapping.scroll_docs(4),
    ['<C-f>'] = cmp.mapping.scroll_docs(-4),
    ['<C-Space>'] = cmp.mapping.complete {},
    ['<C-l>'] = cmp.mapping(function()
      if luasnip.expand_or_locally_jumpable() then luasnip.expand_or_jump() end
    end, { 'i', 's' }),
    ['<C-h>'] = cmp.mapping(function()
      if luasnip.locally_jumpable(-1) then luasnip.jump(-1) end
    end, { 'i', 's' }),
  },
  cmdline = {
    { '/', '?' },
    {
      mapping = cmp.mapping.preset.cmdline(),
      sources = cmp.config.sources {
        { name = 'nvim_lsp_document_symbol' },
        { name = 'buffer' },
      },
    },
    { ':' },
    {
      mapping = cmp.mapping.preset.cmdline(),
      sources = cmp.config.sources {
        { name = 'path' },
        { name = 'cmdline' },
      },
      matching = { disallow_symbol_nonprefix_matchng = false },
    },
  },
}

require('cmp').setup.filetype({
  'dap-repl',
}, { sources = {
  { name = 'dap' },
} })
