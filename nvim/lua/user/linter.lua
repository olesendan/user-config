vim.notify("Load linter config", vim.log.levels["INFO"])

-- NOTE: python related linters are managed with pyenv for the user
-- 'mypy',
-- 'pylint',
-- 'ruff'
local linters_to_install = {
	"luacheck",
	"shellcheck",
	"markdownlint",
	"jsonlint",
	"hadolint",
	"proselint",
	-- 'jq',
	"codespell",
	-- 'cbfmt',
	-- 'prettierd',
	-- 'shellharden',
}
require("mason").setup({})
local registry = require("mason-registry")
registry.refresh(function()
	for _, name in pairs(linters_to_install) do
		vim.print("Checking linter " .. name)
		local package = registry.get_package(name)
		if not registry.is_installed(name) then
			vim.print("Installing linter " .. name)
			package:install()
		end
	end
end)
local linter_args = {
	["mypy"] = {
		["default"] = {},
		["gf"] = {
			"--show-column-numbers",
			"--show-error-end",
			"--hide-error-codes",
			"--hide-error-context",
			"--no-color-output",
			"--no-error-summary",
			"--no-pretty",
			"--install-types",
			"--explicit-package-bases",
			"--namespace-packages=True",
		},
	},
	["pycodestyle"] = {
		["default"] = {},
		["gf"] = {
			"--format=%(path)s:%(row)d:%(col)d:%(code)s:%(text)s",
			"--max-line-length=140",
			"--ignore=error",
			"--max-doc-length=140",
			"--statistics",
			"-",
		},
	},
}

local toggle_linters = function(lint)
	for linter, args in pairs(linter_args) do
		if next(args.default) == nil then
			args.default = lint.linters[linter].args
		end
		if lint.linters[linter].args == args.default then
			vim.print("setting gf args")
			lint.linters[linter].args = args.gf
		else
			vim.print("setting default args")
			lint.linters[linter].args = args.default
		end
	end
end
local lint = require("lint")
lint.linters.luacheck.args = {
	"--formatter",
	"plain",
	"--codes",
	"--ranges",
	"--globals",
	"vim",
	-- 'lvim',
	-- 'reload',
	"-",
}
lint.linters.pylint.cmd = "python"
lint.linters.pylint.args = { "-m", "pylint", "-f", "json" }
lint.linters_by_ft = {
	dockerfile = { "hadolint", "codespell" },
	json = { "jsonlint", "codespell" },
	markdown = { "markdownlint", "proselint", "codespell" },
	lua = { "luacheck", "codespell" },
	python = {
		"pylint",
		"mypy",
		"ruff",
		"codespell",
	},
	text = { "proselint", "codespell" },
	sh = { "shellcheck", "codespell" },
}

local lint_augroup = vim.api.nvim_create_augroup("lint", { clear = true })
vim.api.nvim_create_autocmd({ "BufEnter", "BufWritePost", "InsertLeave" }, {
	group = lint_augroup,
	callback = function()
		lint.try_lint()
	end,
})

local wk = require("which-key")
wk.register({
	["<leader>l"] = { name = "Lint" },
})
vim.keymap.set("n", "<leader>ll", function()
	toggle_linters(lint)
end, { desc = "Toggle lint args (default / gf)" })
