vim.notify('Loading diagnostic config', vim.log.levels['INFO'])

local trouble = require 'trouble'
trouble.setup {}
local key = function(mode, lhs, rhs, opts)
  opts.desc = 'DIAG ' .. opts.desc
  lhs = '<leader>d' .. lhs
  vim.keymap.set(mode, lhs, rhs, opts)
end
local nkey = function(lhs, rhs, opts) key('n', lhs, rhs, opts) end
local wk = require 'which-key'
wk.register { ['<leader>d'] = { name = '[D]iagnostic' } }
nkey('e', vim.diagnostic.open_float, { desc = 'Open error messages' })
nkey('l', vim.diagnostic.setloclist, { desc = 'Open quicklist' })
nkey('n', vim.diagnostic.goto_next, { desc = 'Go to next diagnostic' }) -- alt. to ]d
nkey('p', vim.diagnostic.goto_prev, { desc = 'Go to previous diagnostic' }) -- alt. to [d
wk.register { ['<leader>dt'] = { name = '[T]rouble' } }
nkey('tD', '<CMD>Trouble lsp toggle focus=false win.position=right<CR>', {
  desc = 'lsp definitions',
})
nkey('tl', '<CMD>Trouble loclist toggle<CR>', {
  desc = 'location list',
})
nkey('tq', '<CMD>Trouble qflist toggle<CR>', {
  desc = 'toggle Trouble quickfix list',
})
nkey('ts', '<CMD>Trouble symbols toggle focus=false<CR>', {
  desc = 'LSP symbols',
})
wk.register { ['<leader>dtd'] = { name = 'Diagnostics' } }
nkey('tdb', '<CMD>Trouble diagnostics toggle filter.buf=0<CR>', {
  desc = 'diagnostics for buffer',
})
nkey('tdw', '<CMD>Trouble diagnostics toggle<CR>', {
  desc = 'diagnostics workspace',
})
