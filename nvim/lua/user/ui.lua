vim.notify('Load ui configs', vim.log.levels['INFO'])

local ibl = require 'ibl'
ibl.setup {
  indent = {},
  whitespace = {
    remove_blankline_trail = true,
  },
  scope = {
    enabled = true,
    show_exact_scope = true,
    show_start = true,
    show_end = true,
  },
  exclude = {
    filetypes = {
      'lspinfo',
      'checkhealth',
      'help',
      'man',
      'gitcommit',
      'markdown',
      'text',
    },
    buftypes = {
      'terminal',
      'prompt',
      'nofile',
      'help',
      'quickfix',
      'loclist',
    },
  },
}
local hooks = require 'ibl.hooks'
hooks.register(
  hooks.type.WHITESPACE,
  hooks.builtin.hide_first_space_indent_level
)
