vim.notify('Loading options', vim.log.levels['INFO'])

-- vim.cmd [[colorscheme habamax]]
vim.cmd [[set t_Co=256]]
vim.cmd 'language en_US.utf8'
vim.keymap.set(
  { 'n', 'v' },
  '<Space>',
  '<Nop>',
  { desc = 'Unsetting Space before setting as leader' }
)
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

vim.g.have_nerd_font = true

local opt = vim.opt

opt.background = 'dark'
opt.title = false

opt.backup = false
opt.writebackup = true
opt.backupcopy = 'yes'
opt.undofile = true
opt.undolevels = 20000
opt.encoding = 'utf-8'
opt.fileencoding = 'utf-8'
opt.shada = { "'1000", '<500', 'f1', ':500', 'h' }
opt.confirm = true
opt.mouse = 'a'
opt.updatetime = 250
opt.timeoutlen = 300
opt.clipboard = 'unnamedplus'
opt.showmode = false
opt.splitright = true
opt.splitbelow = true
opt.winminwidth = 5
opt.winheight = 2 -- has to be increased because of winminheight
opt.winminheight = 2

opt.ignorecase = true
opt.smartcase = true
opt.hlsearch = true

-- TODO: implement lua line or mini statusline
opt.laststatus = 2
-- opt.cmdheight = false
-- opt.showmode = false
-- opt.showcmd = false
-- opt.ruler = false

opt.formatoptions:remove 'o'
opt.signcolumn = 'yes'
opt.inccommand = 'split'
opt.number = true
opt.relativenumber = true
opt.signcolumn = 'yes'
opt.numberwidth = 1
opt.cursorline = true
opt.cursorcolumn = true
opt.cursorlineopt = 'number,screenline'
opt.scrolloff = 80
opt.sidescrolloff = 10
opt.splitkeep = 'screen'
opt.colorcolumn = '120'
opt.linebreak = true
opt.breakindent = true

opt.wrap = true -- maybe set to false
opt.whichwrap:append '<,>,[,]' -- NOTE: maybe add l,h
opt.smarttab = true
opt.expandtab = true
opt.tabstop = 3
opt.shiftwidth = 3
opt.softtabstop = 3
opt.foldmethod = 'indent'
opt.foldlevelstart = 1
opt.foldcolumn = 'auto'

opt.breakindent = true
opt.shiftround = true
opt.autoindent = true
opt.smartindent = true
opt.copyindent = true
opt.preserveindent = true

opt.list = true
opt.listchars:append {
  tab = '-> ',
  trail = '*',
  nbsp = '_',
  precedes = '<',
  extends = '>',
  eol = '<',
}
opt.fillchars:append {
  eob = ' ',
  diff = '/',
  fold = ' ',
  foldopen = '>',
  foldclose = '<',
  foldsep = '|',
  vert = '|',
  horiz = '-',
}
opt.shell = '/bin/bash'

opt.grepprg = 'rg --vimgrep --no-heading --smart-case'
opt.grepformat = '%f:%l:%c:%m,%f:%l:%m'

opt.diffopt = 'internal,filler,vertical,closeoff,linematch:60'

opt.wildmode = 'longest:full,full'

opt.completeopt = { 'menu,menuone,noinsert,preview' } -- maybee change preview to popup
opt.pumblend = 10
opt.pumheight = 10

opt.iskeyword:append '-'

opt.isfname:append { '@-@' }
