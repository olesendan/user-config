vim.notify("Load formatting config", vim.log.levels["INFO"])

-- ruff is installed in pyenv setup.
local formatters_to_install = {
	"stylua",
	"shellcheck",
	"beautysh", -- shell formatter
	-- "markdownfmt",
	"markdownlint",
	"markdown-toc",
	"mdformat",
	"mdsf",
	"shfmt",
	"docformatter",
	"prettierd",
	-- 'jq',
	-- 'codespell',
	-- 'cbfmt',
	-- 'mdsf',
	-- 'shellharden',
}

require("mason").setup({})
local registry = require("mason-registry")
registry.refresh(function()
	for _, name in pairs(formatters_to_install) do
		vim.print("Checking formatter " .. name)
		local package = registry.get_package(name)
		if not registry.is_installed(name) then
			vim.print("Installing formatter " .. name)
			package:install()
		end
	end
end)
require("conform").setup({
	format_on_save = function()
		return {
			timeout_ms = 500,
			lsp_fallback = true,
		}
	end,
	formatters_by_ft = {
		lua = { "stylua" },
		sh = { "shfmt", "shellcheck" },
		markdown = { "markdownlint", "markdown-toc" },
		python = { "ruff_format" },
	},
})

vim.keymap.set("n", "<leader>bf", function()
	require("conform").format({
		async = true,
		lsp_fallback = true,
	})
end, { desc = "fmt: Format buffer" })
