vim.notify("Loading autocommands", vim.log.levels["INFO"])

local highlight_group = vim.api.nvim_create_augroup("YankHighlight", { clear = true })
vim.api.nvim_create_autocmd("TextYankPost", {
	group = highlight_group,
	pattern = "*",
	callback = function()
		vim.highlight.on_yank()
	end,
})

local filetype_group = vim.api.nvim_create_augroup("FileTypeGrp", { clear = true })
vim.api.nvim_create_autocmd({ "BufEnter", "BufNewFile" }, {
	group = filetype_group,
	pattern = "*.bash.local",
	command = "set filetype=sh",
})
