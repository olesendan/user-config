vim.notify("Loading keymaps", vim.log.levels["INFO"])
local set = vim.keymap.set
local key = function(mode, lhs, rhs, opts)
	vim.keymap.set(mode, lhs, rhs, opts)
end
local nkey = function(lhs, rhs, opts)
	key("n", lhs, rhs, opts)
end

-- Prefixes for general keymaps is managed in which-key.lua
set("i", "jk", "<Esc>", { desc = "Escape insert mode" })
set("n", "<Esc>", "<CMD>nohlsearch<CR>", { desc = "Remove search highlight" })
set("n", "<CR>", function()
	if vim.opt.hlsearch:get() then
		vim.cmd.nohlsearch()
		return "<CR>"
	else
		return "<CR>"
	end
end, {
	desc = "Toggle Hlsearch",
	expr = true,
})

set("n", "<leader>\\", ":vsplit<CR>", { desc = "Split vertical" })
set("n", "<leader>-", ":split<CR>", { desc = "Split horizontal" })

set("n", "<C-h>", "<C-w>h", { desc = "move buffer focus left" })
set("n", "<C-j>", "<C-w>j", { desc = "move buffer focus down" })
set("n", "<C-k>", "<C-w>k", { desc = "move buffer focus up" })
set("n", "<C-l>", "<C-w>l", { desc = "move buffer focus right" })

nkey("<C-Up>", ":resize +2<CR>", { desc = "Make window taller" })
nkey("<C-Down>", ":resize -2<CR>", { desc = "Make window smaller" })
nkey("<C-Left>", ":vertical resize -5<CR>", { desc = "Make window wider" })
nkey("<C-Right>", ":vertical resize +5<CR>", { desc = "Make window narrower" })

nkey("<leader>bx", "<CMD>source %<CR>", { desc = "Source current buffer" })
nkey("<leader>bw", ":w<CR>", { desc = "Write buffer to disk" })

nkey("k", "v:count == 0 ? 'gk' : 'k'", {
	desc = "move up in wrapped line",
	expr = true,
})
nkey("j", "v:count == 0 ? 'gj' : 'j'", {
	desc = "move down in wrapped line",
	expr = true,
})
nkey("<C-d>", "<C-d>zz", { desc = "jump 1/2 page down" })
nkey("<C-u>", "<C-u>zz", { desc = "jump 1/2 page up" })
nkey("n", "nzzzv", { desc = "jump to next" })
nkey("N", "Nzzzv", { desc = "jump to previous" })
nkey("<leader>q", ":close<CR>", { desc = "Close window" })
nkey("<leader>bd", ":bdelete<CR>", { desc = "Close buffer" })
key("x", "p", [["_dP]], { desc = "Swap line" })
nkey("Y", "y$", { desc = "Yank to system clipboard" })
nkey("<leader>y", "+y", { desc = "Yank to main register" })
key("v", "<", "<gv", { desc = "decrease indent" })
key("v", ">", ">gv", { desc = "increase indent" })
