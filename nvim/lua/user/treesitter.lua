vim.notify('Load treesitter config', vim.log.levels['INFO'])

local configs = require 'nvim-treesitter.configs'
configs.setup {
  ensure_installed = {
    'c',
    'lua',
    'vim',
    'vimdoc',
    'query',
    'dap_repl',
    'bash',
    'comment',
    'diff',
    'dockerfile',
    'jq',
    'json',
    'make',
    'markdown',
    'python',
    'readline',
    'requirements',
    'sql',
    'ssh_config',
    'toml',
    'tmux',
    'yaml',
  },
  highlight = {
    enable = true,
  },
  indent = {
    enable = true,
  },
}
