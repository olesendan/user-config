vim.notify('Loading comment config', vim.log.levels['INFO'])

local wk = require 'which-key'
local todo = require 'todo-comments'
todo.setup {}

vim.keymap.set('n', ']t', todo.jump_next, { desc = 'jump next TODO' })
vim.keymap.set('n', '[t', todo.jump_prev, { desc = 'jump prev TODO' })
wk.register { ['<leader>c'] = { name = '[C]omments' } }
wk.register { ['<leader>ct'] = { name = '[T]odo comments' } }
local ctn = function() todo.jump_next { keywords = { 'TODO' } } end
vim.keymap.set('n', '<leader>ctn', ctn, { desc = 'jump next TODO' })
local ctp = function() todo.jump_prev { keywords = { 'TODO' } } end
vim.keymap.set('n', '<leader>ctp', ctp, { desc = 'jump prev TODO' })
vim.keymap.set('n', '<leader>ctt', '<CMD>TodoTrouble toggle<CR>', {
  desc = 'Toggle TodoTrouble',
})
