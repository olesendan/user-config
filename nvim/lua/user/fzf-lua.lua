vim.notify('Loading fzf-lua config', vim.log.levels['INFO'])
local fzf = require 'fzf-lua'
fzf.setup {}

local wk = require 'which-key'
local key = function(mode, lhs, rhs, opts) vim.keymap.set(mode, lhs, rhs, opts) end
local nkey = function(lhs, rhs, opts) key('n', lhs, rhs, opts) end

-- search
nkey('<leader>/', fzf.lgrep_curbuf, { desc = 'search current buffer' })

wk.register { ['<leader>s'] = { name = '[S]earch' } }
nkey('<leader>s.', fzf.oldfiles, { desc = 'Search for resent files' })
nkey('<leader>s/', fzf.lgrep_curbuf, { desc = 'search current buffer' })
nkey('<leader>sb', fzf.buffers, { desc = 'Search buffers' })
nkey('<leader>sc', fzf.grep_cword, { desc = 'search word under cursor' })
nkey('<leader>sC', fzf.grep_cWORD, { desc = 'search WORD under cursor' })
nkey('<leader>sf', fzf.files, { desc = 'Search for files' })
nkey('<leader>sg', fzf.live_grep_native, { desc = 'search live grep native' })
nkey('<leader>sh', fzf.helptags, { desc = 'search help tags' })
nkey('<leader>sl', fzf.loclist, { desc = 'search loclist' })
nkey('<leader>sp', fzf.grep_project, { desc = 'search project lines' })
nkey('<leader>sq', fzf.quickfix, { desc = 'search quickfix list' })
nkey('<leader>sw', fzf.grep_visual, { desc = 'search visual selection' })
-- nkey('<leader>s?', fzf.grep_curbuf, { desc = 'search current buffer' })
-- diagnostics
-- nkey('<leader>ssq', fzf.quickfix_stack, { desc = 'search stacked quickfix' })
-- nkey('<leader>ssl', fzf.loclist_stack, { desc = 'search stacked loclist' })

-- misc
wk.register {
  ['<leader>hk'] = { name = '[K]eymaps' },
}
nkey('<leader>hc', fzf.commands, { desc = 'nvim commands' })
nkey('<leader>hf', fzf.helptags, { desc = 'Search for help tags' })
nkey('<leader>hh', fzf.command_history, { desc = 'command history' })
nkey('<leader>hkf', fzf.keymaps, { desc = 'fzf help keymaps' })
nkey('<leader>hm', fzf.manpages, { desc = 'Search for man pages' })
nkey('<leader>hr', fzf.resume, { desc = 'resume' })
nkey('<leader>hs', fzf.spell_suggest, { desc = 'spelling' })
nkey('<leader>hx', fzf.menus, { desc = 'menus' })
