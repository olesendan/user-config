vim.notify("Load lsp config", vim.log.levels["INFO"])

local disable_semantic_tokens = {
	lua = true,
}
local wk = require("which-key")
local fzf = require("fzf-lua")
local key = function(mode, lhs, rhs, opts)
	vim.keymap.set(mode, lhs, rhs, opts)
end
local nkey = function(lhs, rhs, opts)
	opts.desc = "LSP: " .. opts.desc
	key("n", lhs, rhs, opts)
end
vim.api.nvim_create_autocmd("LspAttach", {
	group = vim.api.nvim_create_augroup("user-lsp-attach", { clear = true }),
	callback = function(event)
		local bufnr = event.buf
		local client = assert(vim.lsp.get_client_by_id(event.data.client_id), "must have valid client")
		local filetype = vim.bo[bufnr].filetype
		vim.opt_local.omnifunc = "v:lua.vim.lsp.omnifunc"
		wk.register({ ["<leader>l"] = { name = "[L]sp" } }) -- { name = '[L]sp', _ = 'which_key_ignore' },
		nkey("<leader>la", fzf.lsp_code_actions, { desc = "code actions u/ cur" })

		wk.register({ ["<leader>lb"] = { name = "scope [B]uffer" } })
		nkey("<leader>lbd", fzf.lsp_document_diagnostics, { desc = "buf diags" })
		nkey("<leader>lbs", fzf.lsp_document_symbols, { desc = "doc symbols" })

		wk.register({ ["<leader>lc"] = { name = "Calls" } })
		nkey("<leader>lci", fzf.lsp_incoming_calls, { desc = "incoming calls" })
		nkey("<leader>lco", fzf.lsp_outgoing_calls, { desc = "outgoing calls" })

		nkey("<leader>ld", fzf.lsp_definitions, { desc = "definitions" })
		nkey("<leader>lD", fzf.lsp_declarations, { desc = "declarations" })
		nkey("<leader>lf", fzf.lsp_finder, { desc = "finder" })
		nkey("<leader>li", fzf.lsp_implementations, { desc = "implementations" })
		nkey("<leader>lr", fzf.lsp_references, { desc = "references" })
		nkey("<leader>lt", fzf.lsp_typedefs, { desc = "type definitions" })

		wk.register({ ["<leader>lw"] = { name = "scope [W]orkspace" } })
		nkey("<leader>la", fzf.lsp_code_actions, { desc = "code actions u/ cur" })
		nkey("<leader>lwd", fzf.lsp_workspace_diagnostics, { desc = "ws diags" })
		nkey("<leader>lws", fzf.lsp_live_workspace_symbols, {
			desc = "ws symb liv",
		})
		nkey("<leader>lwS", fzf.lsp_workspace_symbols, { desc = "ws symbols" })

		-- General mappings for lsp
		nkey("gd", vim.lsp.buf.definition, { desc = "definition", buffer = 0 })
		nkey("gD", vim.lsp.buf.declaration, { desc = "declaration", buffer = 0 })
		nkey("gI", fzf.lsp_implementations, {
			desc = "fzf implementation",
			buffer = 0,
		})
		nkey("gr", vim.lsp.buf.references, { desc = "references", buffer = 0 })
		nkey("gR", fzf.lsp_references, { desc = "fzf references", buffer = 0 })
		nkey("gT", vim.lsp.buf.type_definition, {
			desc = "type definition",
			buffer = 0,
		})
		nkey("<leader>K", vim.lsp.buf.hover, { desc = "hover docs.", buffer = 0 })
		nkey("<leader>cr", vim.lsp.buf.rename, { desc = "buf rename", buffer = 0 })
		nkey("<leader>ca", vim.lsp.buf.code_action, {
			desc = "code action.",
			buffer = 0,
		})
		if disable_semantic_tokens[filetype] then
			client.server_capabilities.semanticTokensProvider = nil
		end
		if client and client.server_capabilities.documentHighlightProvider then
			local highlight_augroup = vim.api.nvim_create_augroup("user-lsp-highlight", { clear = true })
			vim.api.nvim_create_autocmd({ "CursorHold", "CursorHoldI" }, {
				buffer = bufnr,
				group = highlight_augroup,
				callback = vim.lsp.buf.document_highlight,
			})
			vim.api.nvim_create_autocmd({ "CursorMoved", "CursorMovedI" }, {
				buffer = bufnr,
				group = highlight_augroup,
				callback = vim.lsp.buf.clear_references,
			})
			vim.api.nvim_create_autocmd("LspDetach", {
				group = vim.api.nvim_create_augroup("user-lsp-detach", { clear = true }),
				callback = function(event2)
					vim.lsp.buf.clear_references()
					vim.api.nvim_clear_autocmds({
						group = "user-lsp-highlight",
						buffer = event2.buf,
					})
				end,
			})
		end
		local function toggle_hint()
			vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled())
		end
		if client and client.server_capabilities.inlayHintProvider and vim.lsp.inlay_hint then
			nkey("<leader>li", toggle_hint, { desc = "toggle inlay hints" })
		end
	end,
})

local capabilities = vim.lsp.protocol.make_client_capabilities()
if pcall(require, "cmp_nvim_lsp") then
	capabilities = vim.tbl_deep_extend("force", capabilities, require("cmp_nvim_lsp").default_capabilities())
	capabilities.textDocument.completion.completionItem.snippetSupport = true
end

local servers = {
	bashls = {
		mason_name = "bash-language-server",
		settings = {
			bashIde = {
				globpattern = "*@(.sh|.inc|.bash|.command)",
			},
		},
	},
	diagnosticls = {
		mason_name = "diagnostic-languageserver",
		settings = {
			filetypes = { "sh", "email" },
		},
	},
	-- gitlab_ci_ls = {
	--   manual_install = true,
	-- },
	jsonls = {
		settings = {
			json = {
				format = { enable = true },
				schemas = require("schemastore").json.schemas(),
				validate = { enable = true },
			},
		},
	},
	lua_ls = {
		mason_name = "lua-language-server",
		settings = {
			Lua = {
				runtime = { version = "LuaJIT" },
				diagnostics = { globals = { "vim" } },
				workspace = {
					checkThirdParty = false,
					library = {
						vim.env.VIMRUNTIME,
					},
				},
				telemetry = { enable = false },
				completion = {
					callSnippet = "Replace",
				},
			},
		},
	},
	marksman = {
		mason_name = "marksman",
		settings = {},
	},
	prosemd_lsp = {
		mason_name = "prosemd-lsp",
		settings = {},
	},
	-- pyright is managed in user pyenv
	pyright = {
		settings = {
			python = {
				-- disableOrganizeImports = true,
				analysis = {
					indexing = true,
					autoSearchPaths = true,
					diagnosticMode = "workspace", -- NOTE: maybe 'openFilesOnly',
					useLibraryCodeForTypes = true,
					autoImportCompletions = true,
					include = { "src", "./src" },
					extraPaths = {
						"src",
						"./src",
					},
					exclude = { ".vscode", ".git", ".github" },
					typeCheckingMode = "standard", -- NOTE: maybe 'basic'
				},
			},
		},
	},
	-- pylsp = {
	--   settings = {
	--     pylsp = {
	--       configurationSources = {
	--         'pycodestyle',
	--         'pylint',
	--         'pydocstyle',
	--       },
	--       plugins = {
	--         pycodestyle = {
	--           enabled = true,
	--           maxLineLength = 140,
	--         },
	--         pylint = {
	--           enabled = true,
	--           args = {
	--             '--max-line-length=140',
	--           },
	--         },
	--         pydocstyle = {
	--           enabled = true,
	--           convention = 'pep257',
	--         },
	--       },
	--     },
	--   },
	-- },
	yamlls = {
		settings = {
			yaml = {
				schemaStore = {
					enable = false,
					url = "",
				},
				schemas = require("schemastore").yaml.schemas(),
			},
		},
	},
}
local get_mason_name = function(table)
	local i = 1
	local result = {}
	for _, v in pairs(table) do
		if v.mason_name then
			result[i] = v.mason_name
			i = i + 1
		end
	end
	return result
end
local servers_to_install = get_mason_name(servers)
local lspconfig = require("lspconfig")
require("mason").setup({})
local registry = require("mason-registry")
registry.refresh(function()
	for _, name in pairs(servers_to_install) do
		vim.print("checking lsp " .. name)
		local package = registry.get_package(name)
		if not registry.is_installed(name) then
			vim.print("installing lsp " .. name)
			package:install()
		end
	end
end)
for name, config in pairs(servers) do
	config = vim.tbl_deep_extend("force", {}, {
		capabilities = capabilities,
	}, config.settings)
	lspconfig[name].setup(config)
end

vim.g.diagnostics_text_active = true
local toggle_lsp_diag = function()
	if vim.g.diagnostics_text_active then
		vim.g.diagnostics_text_active = false
		vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(vim.lsp.diagnostic.on_publish_diagnostics, {
			underline = true,
			virtual_text = false,
			signs = true,
			update_in_insert = true,
		})
	else
		vim.g.diagnostics_text_active = true
		vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(vim.lsp.diagnostic.on_publish_diagnostics, {
			underline = true,
			virtual_text = true,
			signs = true,
			update_in_insert = true,
		})
	end
end
nkey("<leader>lz", function()
	toggle_lsp_diag()
end, { desc = "toggle lsp text" })
