vim.notify("Loading Lazy", vim.log.levels["INFO"])

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable",
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
	spec = {
		{ import = "user/plugins" },
		{ import = "user/plugins/local" },
	},
	ui = {
		border = "single",
	},
	checker = {
		enabled = true,
		notify = false,
	},
	change_detection = {
		enabled = true,
		notify = false,
	},
	install = { colorscheme = { "tokyonight", "habamax" } },
	performance = {
		cache = { enabled = true },
		rtp = {
			disabled_plugins = {
				-- 'rplugin',
				"netrwPlugin",
			},
		},
	},
})
