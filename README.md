# DO User Configuration

This repo is for setting up a opionated user on a linux machine.

## Usage

Clone the repo to the relevant machine, into ~/.config. It expects that the user is new.

### Python

uses pip-tools and pipx for configuration. There is a few aliases for maintenance
p_compile: creates the requirements.txt file in .local/state/python/, from ./.config/python/requirements.in
p_sync: recreates the python env from ./local/state/python/requirements.txt
