#!/usr/bin/bash
# enable pyenv
# printf "%s\n" "Sourcing pyenv.bash"
if command -v pyenv; then
	echo "pyenv exists"
	export PYENV_ROOT="$HOME/.local/share/pyenv"
	command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
	eval "$(pyenv init -)"
else
	echo "can not find pyenv"
fi

function python_reset_packages() {
	pip freeze | xargs pip uninstall -y
}

function python_pip_add_pack() {
	nvim "$HOME"/.config/python/requirements.in.local
}

function python_pip_compile() {
	if command -v pip-compile >/dev/null; then
		printf "%s\n" "creating .local/share/python/requirements.txt"
		pip-compile -o "$HOME"/.local/share/python/requirements.txt "$HOME"/.config/python/requirements.in "$HOME"/.config/python/requirements.in.local
	else
		printf "%s\n" "pip-compile was not found in path"
		return 1
	fi
}

function python_pip_sync() {
	if ! command -v pip-sync >/dev/null; then
		printf "%s\n" "pip-sync not found in path"
		return 1
	else
		if [[ ! -f "$HOME"/.local/share/python/requirements.txt ]]; then
			printf "%s\n" "requirements.txt not found"
		else
			printf "%s\n" "running pip-sync $HOME/.local/share/python/requirements.txt"
			pip-sync "$HOME"/.local/share/python/requirements.txt
		fi
	fi
}

function python_activate_venv() {
	venv_cfg_filepath=$(find . -maxdepth 2 -type f -name 'pyvenv.cfg' 2>/dev/null)
	if [[ -z "$venv_cfg_filepath" ]]; then
		printf "%s\n" "No virtual env found"
		return
	fi
	venv_filepath=$(cut -d '/' -f -2 <<<"${venv_cfg_filepath}")
	if [[ -d $venv_filepath ]]; then
		printf "%s\n" "Activating virtual env"
		source "${venv_filepath}"/bin/activate
	fi
}

function python_deactivate_venv() {
	local cur_dir=""
	cur_dir=$(pwd -P)
	local venv_dir=""
	venv_dir=$(dirname "$VIRTUAL_ENV")
	if [[ "$cur_dir"/ != "$venv_dir"/* ]]; then
		printf "%s\n" "deactivating virtual env"
		deactivate
	fi
}
function python_activated_executable() {
	printf "%s\n" "Activated $(which python)."
}

function cd_activate_venv() {
	builtin cd "$@" || exit
	if [[ -z "$VIRTUAL_ENV" ]]; then
		python_activate_venv
		python_activated_executable
	else
		python_deactivate_venv
	fi
}

function python_pyright_venv_setup() {
	echo '{ "venvPath": ".", "venv": ".venv", "extraPaths": ["./src"] }' >>./pyrightconfig.json
}

alias p_pack_add="python_pip_add_pack"
alias p_compile="python_pip_compile"
alias p_sync="python_pip_sync"

alias p_pyright="python_pyright_venv_setup"

alias pya="python_activate_venv; python_activated_executable"
alias pyd="python_deactivate_venv; python_activated_executable"

# prompt command should activate virtual env if needed.
pyenv_prompt_command() {
	if [ "$SSH_CONNECTION" ]; then
		PS1=" ${GREEN}\u${RESET}@${BOLD}${BLUE}\h${RESET}${BOLD}\$ ${RESET}"
	else
		PS1=" ${GREEN}${BOLD}\$ ${RESET}"
	fi
	if [[ -z "$VIRTUAL_ENV" ]]; then
		python_activate_venv
		python_activated_executable
	else
		python_deactivate_venv
		python_activated_executable
	fi
	if [ ! "$TMUX" ]; then
		PS1='${VIRTUAL_ENV_PROMPT}   ${PWD}   $(__git_ps1 "(%s)")\n'$PS1
	else
		PS1='${VIRTUAL_ENV_PROMPT}   $(__git_ps1 "(%s)")\n'$PS1
	fi
}

PROMPT_COMMAND=pyenv_prompt_command
printf "%s\n" "pyenv.bash sourced"
