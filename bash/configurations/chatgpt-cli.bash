# configurations for chatgpt-cli
# printf "%s\n" "Sourcing chatgpt-cli.bash"
[[ $PS1 && -f /usr/local/bin/chatgpt ]] && source <(chatgpt --set-completions bash)

function gpt_git_commit() {
	declare commit_message
	declare query
	git_br_current="$(git branch --show-current)"
	git_br_default="$(basename "$(git rev-parse --abbrev-ref origin/HEAD)")"
	merge_diff="${git_br_default}"..HEAD
	git_log="${git_br_default}"..HEAD
	query_diff="Generate git commit message from git diff, using semantic versioning. Start your answer with the actual git commit message. The commit title should include areas in parentheses, and have an empty line after it. End this section with a new line containing six dashes. Provide all your thoughts and explainsions in the next section, ending with a new line containing six dashes. If there is any breaking changes, then explain them in the last line in your answer starting with breaking change in upper case, otherwise omit this. My diff: "
	query_log="Generate git commit message from git log, using semantic versioning. Start your answer with the actual git commit message. The commit title should include areas in parentheses, and have an empty line after it. End this section with a new line containing six dashes. Provide all your thoughts and explainsions in the next section, ending with a new line containing six dashes. If there is any breaking changes, then explain them in the last line in your answer starting with breaking change in upper case, otherwise omit this. My log: "
	case $1 in
	"merge")
		printf "%s\n" "diff default branch to head"
		gpt_message="$(AZURE_ROLE="I want you to act as a bash, python and golang programming mentor, helping me with my coding tasks and expanding my knowledge. Share tips, tricks, and guiding principles to improve my skills and make my code more efficient. Explain common errors or challenges I might encounter along the way and provide examples or exercises to practice different programming concepts. Help me understand best practices and design patterns in bash, python and golang and encourage me to explore advanced topics such as working with libraries and frameworks." git diff "${merge_diff}" | chatgpt --query "${query_diff}")"
		;;
	"log")
		printf "%s\n" "Log default to head"
		gpt_message="$(AZURE_ROLE="I want you to act as a bash, python and golang programming mentor, helping me with my coding tasks and expanding my knowledge. Share tips, tricks, and guiding principles to improve my skills and make my code more efficient. Explain common errors or challenges I might encounter along the way and provide examples or exercises to practice different programming concepts. Help me understand best practices and design patterns in bash, python and golang and encourage me to explore advanced topics such as working with libraries and frameworks." git log "${git_log}" | chatgpt --query "${query_log}")"
		;;
	*)
		printf "%s\n" "diff Head"
		gpt_message="$(AZURE_ROLE="I want you to act as a bash, python and golang programming mentor, helping me with my coding tasks and expanding my knowledge. Share tips, tricks, and guiding principles to improve my skills and make my code more efficient. Explain common errors or challenges I might encounter along the way and provide examples or exercises to practice different programming concepts. Help me understand best practices and design patterns in bash, python and golang and encourage me to explore advanced topics such as working with libraries and frameworks." git diff | chatgpt --query "${query_diff}")"
		;;
	esac

	commit_message=$(echo "$gpt_message" | sed '1{/^###/d}')
	commit_message=$(echo "$commit_message" | sed '1{/^**/d}')
	commit_message=$(echo "$commit_message" | sed '1{/^$/d}')
	commit_message=$(echo "$commit_message" | sed '/```/d')
	commit_message=$(echo "$commit_message" | sed '1s/$/\n/')
	printf "%s\n" "$commit_message"
	read -r -p "Do you want to create a git commit from the query? [y/N] " response
	if [[ $response =~ ^[Yy]$ ]]; then
		git add . && git commit --allow-empty -m "$commit_message"
	else
		printf "%s\n" "Commit cancelled."
	fi
}

alias gpt='chatgpt'
alias gpti='chatgpt --interactive'
alias gptc="AZURE_ROLE='I want you to act as a bash, python and golang programming mentor, helping me with my coding tasks and expanding my knowledge. Share tips, tricks, and guiding principles to improve my skills and make my code more efficient. Explain common errors or challenges I might encounter along the way and provide examples or exercises to practice different programming concepts. Help me understand best practices and design patterns in bash, python and golang and encourage me to explore advanced topics such as working with libraries and frameworks.' chatgpt --interactive"
alias ggpt="gpt_git_commit"

printf "%s\n" "Sourced chatgpt-cli.bash"
