#!/usr/bin/bash
# bash configurations for tmux-bash-completion
# printf "%s\n" "Sourcing tmux-bash-completion.bash"
[[ $PS1 && -f /usr/share/bash-completion/bash_completion_tmux ]] && source /usr/share/bash-completion/completions/tmux
printf "%s\n" "Sourced tmux-bash-completion.bash"
