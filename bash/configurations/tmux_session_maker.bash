#!/usr/bin/bash
# bash configurations for tmux_session_maker
# printf "%s\n" "Sourcing tmux_session_maker.bash"
alias ta='tmux_session_maker'
alias tac='tmux_session_maker $HOME/.config'
printf "%s\n" "Sourced tmux_session_maker.bash"
