#!/usr/bin/bash
# Bash configuration of bash-completion
# printf "%s\n" "Sourcing bash-completion.bash"
[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] && source /usr/share/bash-completion/bash_completion
printf "%s\n" "Sourced bash-completion.bash"
