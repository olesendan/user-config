#!/usr/bin/bash
#
# do bashrc from ~/.config/bash/rc
#

printf "%s\n" "Sourcing Bash/rc"
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

bind -m vi-insert '"jk": vi-movement-mode'
shopt -s histappend
shopt -s checkwinsize
export HISTCONTROL=erasedups:ignoreboth:ignorespace
export HISTSIZE=10000
export HISTFILESIZE=20000
shopt -s autocd

if [[ -f /usr/share/git/completion/git-prompt.sh ]]; then
	source /usr/share/git/completion/git-prompt.sh
	export GIT_PS1_DESCRIBE_STYLE='contains'
	export GIT_PS1_SHOWDIRTYSTATE=1
	export GIT_PS1_SHOWSTASHSTATE=1
	export GIT_PS1_SHOWUNTRACKEDFILES=1
	export GIT_PS1_SHOWUPSTREAM="auto verbose"
	export GIT_PS1_SHOWCONFLICTSTATE="yes"
	export GIT_PS1_SHOWCOLORHINTS=1
fi

RESET="\[$(tput sgr0)\]"
BLUE="\[$(tput setaf 4)\]"
GREEN="\[$(tput setaf 2)\]"
BOLD="\[$(tput bold)\]"

prompt_command() {
	if [ "$SSH_CONNECTION" ]; then
		PS1=" ${GREEN}\u${RESET}@${BOLD}${BLUE}\h${RESET}${BOLD}\$ ${RESET}"
	else
		PS1=" ${GREEN}${BOLD}\$ ${RESET}"
	fi

	if [ ! "$TMUX" ]; then
		PS1='${VIRTUAL_ENV_PROMPT}   ${PWD}   $(__git_ps1 "(%s)")\n'$PS1
	else
		PS1='${VIRTUAL_ENV_PROMPT}   $(__git_ps1 "(%s)")\n'$PS1
	fi
}

PROMPT_COMMAND=prompt_command

# generic functions
function ls_clear_screen() {
	clear
	if [[ -n "$1" ]]; then
		command ls -lh --color=auto --group-directories-first "$1" || return
	else
		command ls -lh --color=auto --group-directories-first "./" || return
	fi
	echo
}

function ls_hidden_clear_screen() {
	clear
	if [[ -n "$1" ]]; then
		command ls -lha --color=auto --group-directories-first "$1" || return
	else
		command ls -lha --color=auto --group-directories-first "./" || return
	fi
	echo
}

# generic aliases
alias su='su -l'
alias c='clear'
alias lc='ls_clear_screen'
alias lca='ls_hidden_clear_screen'
alias ls='ls -lh --color=auto'
alias lsa='ls -alh --color=auto'
alias grep='grep --color=auto'
alias cd..='cd ..'
alias cdc='cd $HOME/.config'

# Show info about the system
[[ -f /usr/bin/neofetch ]] && neofetch

# load application specific configurations
# put machine specific bash files in here, ending with .local
if [[ -d "$HOME"/.config/bash/configurations ]]; then
	for file in "$HOME"/.config/bash/configurations/*; do
		source "$file"
	done
fi

printf "%s\n" "Sourced Bash/rc"
