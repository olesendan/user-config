#!/usr/bin/bash
# Script for setting user defaults.

CONFIG="$HOME"/.config
DATA="$HOME"/.local/share
CACHE="$HOME"/.cache/
BAK="$DATA"/do-user/backup
TIMESTAMP=$(date +"%Y%m%d-%H%M%S")

source_bash_config() {
    printf "%s\n" "Sourcing bash config files."
    set -o allexport
    source "$HOME"/.bash_profile
    set +a
}

setup_env() {
    printf "%s\n" "Creating folders for do user."
    mkdir -pv "$CACHE"
    mkdir -pv "$DATA"
    mkdir -pv "$BAK"
    mkdir -pv "$HOME"/.local/bin
    mkdir -pv "$HOME"/codebases

}

setup_bash() {
    printf "%s\n" "Setup bash"
    if [[ ! -L $HOME/.bash_profile ]]; then
        printf "%s\n" "Links do not exist"
        mv "$HOME"/.bash_profile "$BAK"/
        ln -s "$CONFIG"/bash/profile "$HOME"/.bash_profile
        mv "$HOME"/.bashrc "$BAK"/
        ln -s "$CONFIG"/bash/rc "$HOME"/.bashrc
        mv "$HOME"/.bash_logout "$BAK"/
        ln -s "$CONFIG"/bash/logout "$HOME"/.bash_logout
    else
        printf "%s\n" "Links exists"
    fi

    mkdir -pv "$CACHE"/bash
    if [[ -f "$HOME/.bash_history" ]]; then
        printf "%s\n" ".bash_history exists."
        mv "$HOME"/.bash_history "${CACHE}"/bash/history
    else
        printf "%s\n" "Creating bash/history."
        touch "${CACHE}/bash/history"
    fi
}

setup_gapra() {
    if [[ ! -d "$HOME"/gapra ]]; then
        mkdir -p "$HOME"/gapra/{01-goals,02-areas-of-focus,03-projects,04-rescources,05-archives}
        cp "$CONFIG"/user-data/gapra/gitignore "$HOME"/gapra/.gitignore
        cd "$HOME"/gapra || return

        if [[ ! -d ./.git ]]; then
            git init
            git add .
            git commit -m"initial gapra method"
        fi
        cp -r "$CONFIG"/user-data/gapra/templates "$HOME"/gapra/resources/productivity/
    fi
}

backup_setup() {
    local backup_dir="$BAK"/"$TIMESTAMP"
    mkdir -pv "$backup_dir"
    find "$CONFIG" \( -path "*/user-data" -o -path "*/.git*" -o -name "user-setup.sh" \) -prune -o -print0 | xargs -0 cp -r --parents -t "$backup_dir"
}

install_and_setup_packages_arch() {
    local files_location="$CONFIG"/user-data/packages/arch
    local packages
    for file in "$files_location"/*; do
        [[ -e $file ]] || continue
        pkgname="${file//.local/""}"
        pkgname="${pkgname//.sh/""}"
        packages+=("$(basename "$pkgname")")
    done
    # printf "%s\n" "${packages[@]}"
    sudo pacman -Syu
    sudo pacman -S "${packages[@]}"
    for file in "$files_location"/*; do
        [[ -e $file ]] || continue
        clear
        # printf "%s\n" "$file"
        chmod +x "$file"
        source "$file"
        chmod -x "$file"
    done
}

install_and_setup_packages_aur() {
    local files_location="$CONFIG"/user-data/packages/aur
    local packages
    local start_dir=$PWD
    local build_dir="$DATA"/packages
    mkdir -pv "$build_dir"
    for file in "$files_location"/*; do
        [[ -e $file ]] || continue
        pkgname="${file//.local/""}"
        pkgname="${pkgname//.sh/""}"
        packages+=("$(basename "$pkgname")")
    done
    # printf "%s\n" "${packages[@]}"
    cd "$build_dir" || return 1
    for package in "${packages[@]}"; do
        # echo "$package"
        if [[ -d "$package" ]]; then
            cd "$package" || return 1
            git pull
        else
            git clone "https://aur.archlinux.org/$package.git"
            cd "$package" || return 1
        fi
        makepkg -sic
        cd ../
    done
    cd "$start_dir" || return 1

    for file in "$files_location"/*; do
        [[ -e $file ]] || continue
        clear
        # printf "%s\n" "$file"
        chmod +x "$file"
        source "$file"
        chmod -x "$file"
    done
}

install_and_setup_packages_src() {
    local files_location="$CONFIG"/user-data/packages/src
    local start_dir="$PWD"
    local build_dir="$DATA"/packages
    mkdir -pv "$build_dir"

    # echo "$start_dir"
    for file in "$files_location"/*; do
        [[ -e $file ]] || continue
        clear
        printf "%s\n" "$file"
        chmod +x "$file"
        # echo "$PWD"
        cd "$build_dir" || return
        # echo "$PWD"
        source "$file"
        chmod -x "$file"
        cd "$build_dir" || return
        # echo "$PWD"
    done
    cd "$start_dir" || return
    # echo "$PWD"
}

main() {
    printf "%s\n" "Setting user config"
    printf "%s\n" "Input is: $1"
    setup_env
    setup_bash
    source_bash_config
    backup_setup
    install_and_setup_packages_arch
    install_and_setup_packages_aur
    install_and_setup_packages_src
    setup_gapra
}

if [ "${BASH_SOURCE[0]}" -ef "$0" ]; then
    printf "%s\n" "Script is being executed"
    main
else
    printf "%s\n" "Script is being sourced!"
fi
